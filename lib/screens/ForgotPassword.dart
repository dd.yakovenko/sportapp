import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:squadre/screens/Profile.dart';
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/utils/Firebase.dart' as fbUtils;

class ForgotPassword extends StatefulWidget {

  ForgotPassword();

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  TextEditingController emailController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        color: Utils.colorWhite,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 100.0),
                child: Image.asset('assets/images/football.png'),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Text(
                  "Forgot Password",
                  style: TextStyle(fontSize: 35),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 30, left: 10, right: 10),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    TextField(
                      decoration: InputDecoration(
                          hintText: "Enter email",
                          hintStyle: TextStyle(fontSize: 20),
                          labelText: "Email",
                          labelStyle: TextStyle(color: Utils.blackColor),
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Utils.greyColor))),
                      controller: emailController,
                      style: TextStyle(fontSize: 23),
                    ),
                  ],
                ),
              ),
              Container(
                width: 300,
                height: 50,
                margin: EdgeInsets.only(top: 30),
                child: RaisedButton(
                    onPressed: () {
                      _sendLink(emailController.text);
                    },
                    child: Text(
                      "Reset Password",
                      style: TextStyle(color: Utils.colorWhite, fontSize: 20),
                    ),
                    color: Utils.buttonColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0))),
              ),
              SizedBox(height: 16,)
            ],
          ),
        ),
      ),
    );
  }

  void _sendLink(String email) {

    fbUtils.forgotPassword(email).then((value) {
      if (value) {
        Utils.showMessage("Reset Password Link sent Successfully!\nCheck your email.");
        Navigator.pop(context);
      } else {
        print(Exception);

      }
    }).catchError((error) {
      print(error);
      Utils.showMessage(error.message);
    });
  }
}
