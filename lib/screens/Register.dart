import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:squadre/screens/Profile.dart';
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/utils/Firebase.dart' as fbUtils;

import 'VerificationPending.dart';

class Register extends StatefulWidget {
  // UserModel userModel;

  Register();

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  TextEditingController nameController = TextEditingController();
  TextEditingController surnameController = TextEditingController();
  TextEditingController passController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  var _selectedDate = DateTime.now();
  var _selectedDateString = 'Select date of birthday';
  var _selectedAddress = 'Pick location';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        color: Utils.colorWhite,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 100.0),
                child: Image.asset('assets/images/football.png'),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Text(
                  "Register",
                  style: TextStyle(fontSize: 35),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 30, left: 10, right: 10),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    TextField(
                      decoration: InputDecoration(
                          hintText: "Enter email",
                          hintStyle: TextStyle(fontSize: 20),
                          labelText: "Email",
                          labelStyle: TextStyle(color: Utils.blackColor),
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Utils.greyColor))),
                      controller: emailController,
                      style: TextStyle(fontSize: 23),
                    ),
                    TextField(
                      decoration: InputDecoration(
                          hintText: "Enter name",
                          hintStyle: TextStyle(fontSize: 20),
                          labelText: "Name",
                          labelStyle: TextStyle(color: Utils.blackColor),
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Utils.greyColor))),
                      controller: nameController,
                      style: TextStyle(fontSize: 23),
                    ),
                    TextField(
                      decoration: InputDecoration(
                          hintText: "Enter surname",
                          hintStyle: TextStyle(fontSize: 20),
                          labelText: "Surname",
                          labelStyle: TextStyle(color: Utils.blackColor),
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Utils.greyColor))),
                      controller: surnameController,
                      style: TextStyle(fontSize: 23),
                    ),
                    TextField(
                      decoration: InputDecoration(
                          hintText: "Enter Password",
                          hintStyle: TextStyle(fontSize: 20),
                          labelText: "Password",
                          labelStyle: TextStyle(color: Utils.blackColor),
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Utils.greyColor))),
                      controller: passController,
                      style: TextStyle(fontSize: 23),
                    ),
                    SizedBox(
                      height: 12.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          _selectedDateString,
                          style:
                              TextStyle(color: Utils.blackColor, fontSize: 23),
                        ),
                        IconButton(
                            icon: Icon(Icons.event_note),
                            onPressed: () {
                              _selectDate(context);
                            })
                      ],
                    ),
                    Divider(color: Utils.blackColor),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          _selectedAddress,
                          style:
                              TextStyle(color: Utils.blackColor, fontSize: 23),
                        ),
                        IconButton(
                            icon: new Icon(Icons.add_location),
                            onPressed: _pickAddress)
                      ],
                    ),
                    Divider(color: Utils.blackColor),
                  ],
                ),
              ),
              Container(
                width: 300,
                height: 50,
                margin: EdgeInsets.only(top: 30),
                child: RaisedButton(
                    onPressed: () {
                      register();
                    },
                    child: Text(
                      "Register",
                      style: TextStyle(color: Utils.colorWhite, fontSize: 20),
                    ),
                    color: Utils.buttonColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0))),
              ),
              SizedBox(height: 16,)
            ],
          ),
        ),
      ),
    );
  }

  void register() {
    Utils.registerModel.name = nameController.text;

    fbUtils.register(emailController.text, passController.text, nameController.text, surnameController.text, _selectedDateString, _selectedAddress).then((value) {
      if (value) {
        Utils.registerModel.isVerified = false;
        Utils.showMessage("Activation link sent to your registered email!");
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => VerificationPending()));
      } else {
        print(Exception);
      }
    }).catchError((error) {
      print(error);
      Utils.showMessage(error.message);
    });
  }

  _selectDate(context) async {
    var picked = await showDatePicker(
        context: context,
        initialDate: _selectedDate,
        firstDate: new DateTime(1900),
        lastDate: new DateTime(2100));
    if (picked != null && picked != _selectedDate) {
      setState(() {
        _selectedDate = picked;
        _selectedDateString = DateFormat.yMMMMEEEEd().format(picked);
      });
    }
  }

  _pickAddress() async {
    try {
      /*
      var place = await PluginGooglePlacePicker.showAutocomplete(
          mode: PlaceAutocompleteMode.MODE_OVERLAY);
          */
      final placeName = "";//ù2place.name;
      if (placeName != null) {
        setState(() {
          _selectedAddress = placeName;
        });
      }
    } catch (e) {
      Utils.showMessage(e.message);
    }
  }
}
