import 'package:flutter/material.dart';
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/utils/Firebase.dart' as fbUtils;

class VerificationPending extends StatefulWidget {
  @override
  _VerificationPendingState createState() => _VerificationPendingState();
}

class _VerificationPendingState extends State<VerificationPending> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Account Verification'),
        backgroundColor: Utils.pinkColor,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          color: Utils.colorWhite,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 100.0),
            child: Image.asset('assets/images/football.png'),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Text(
              "Account Verification Pending",
              style: TextStyle(fontSize: 35),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Text(
              "Please check your email for activation link, verify your account to access this app.",
              style: TextStyle(fontSize: 22),
            ),
          ),
          Container(
            width: 300,
            height: 50,
            margin: EdgeInsets.only(bottom: 30, top: 40),
            child: RaisedButton(
                onPressed: () {
                  fbUtils.resendVerificationLink()
                      .then((status){
                    Utils.showMessage("Verification link sent to registered email!");
                  })
                      .catchError((error){
                    Utils.showMessage(error.message);
                  });
                },
                child: Text(
                  "Resend Link",
                  style: TextStyle(color: Utils.colorWhite, fontSize: 20.0),
                ),
                color: Utils.buttonColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0))),
          ),
        ],
      ),
    );
  }
}
