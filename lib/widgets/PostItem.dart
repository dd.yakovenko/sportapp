import 'package:flutter/material.dart';
import 'package:squadre/Model/UserModel.dart';
import 'package:squadre/screens/VideoPreview.dart';
import 'package:squadre/utils/Utils.dart';
import 'package:squadre/utils/AppConstant.dart' as AppConstants;
import 'package:squadre/utils/Firebase.dart' as fbUtils;
import 'package:squadre/widgets/PinderyCircleAvatar.dart';
import 'package:squadre/widgets/video_player/aspect_ratio_video.dart';
import 'package:squadre/widgets/video_player/network_player_lifecycle.dart';
import 'package:video_player/video_player.dart';

class PostItem extends StatelessWidget {
  Map data;
  UserModel _user = UserModel();

  PostItem(this.data) {
    if (data != null) {
      Map<dynamic, dynamic> map = data[AppConstants.USER];
      if (map != null) {
        _user.isAdmin = map[AppConstants.ISADMIN];
        _user.name = map[AppConstants.NAME];
        _user.email = map[AppConstants.EMAIL];
        _user.favoriteTeam = map[AppConstants.FAVORITE_TEAM];
        _user.favoriteTeamName = map[AppConstants.FAVORITE_TEAM_NAME];
        _user.favoriteTeamId = map[AppConstants.FAVORITE_TEAM_ID];
        _user.avatar = map[AppConstants.AVATAR];
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Utils.deviceWidth,
      margin: EdgeInsets.all(10),
      child: Card(
        elevation: 3,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: PinderyCircleAvatar(user: _user, radius: 16),
                  ),
                  Text((_user == null) ? "" : _user.name)
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  data['message'] ?? '',
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: (data['replies'] != null &&
                    data['replies']
                        .values
                        .last['reply']
                        .toString()
                        .contains('http'))
                    ? Container(
                    child: GestureDetector(
                      child: Image.asset('assets/images/video_thumb.jpg'),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VideoPreview(path: data['replies'].values.last['reply']
                                    .toString(),)));
                      },
                    )
                )
                    : Text(
                    data['replies'] == null
                        ? ''
                        : data['replies'].values.last['reply'].toString(),
                    style: TextStyle(
                        fontSize: 15, fontWeight: FontWeight.bold)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
