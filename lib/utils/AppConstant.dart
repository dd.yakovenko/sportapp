import 'package:flutter/material.dart';

//Firebase keys

//Parents
const String USER = "users";
const String POST = "posts";
const String REPLIES = "replies";
//register
const String NAME = "name";
const String USER_ID = "uuid";
const String EMAIL = "email";
const String PASSWORD = "password";
const String SURNAME = "surname";
const String DOB = "dob";
const String LOCATION = "location";
const String ISADMIN = "isAdmin";
const String FAVORITE_TEAM = "favoriteTeam";
const String FAVORITE_TEAM_ID = "favoriteTeamId";
const String FAVORITE_TEAM_NAME = "favoriteTeamName";
const String IS_VERIFIED = "isVerified";

//add posts
const String POST_ID = "postId";
const String TIME = "time";
const String TEAMID = "teamId";
const String MESSAGE = "message";
const String POST_USER_ID = "postUserId";
const String POST_IS_VIDEO = "isVideo";
const String POST_REPLY = "reply";
const String POST_IS_REPLY = "isReplied";
const String POST_REPLY_TIME = "time";

// Teams
const String TEAMS = 'teams';
const String TEAM_NAME = 'name';
const String TEAM_ID = 'id';
const String TEAM_IMAGE = 'image';
const String POST_NUMBER = 'POST_NUMBER';

String apiToken;
//API Key

const String FIRSTNAME = "first_name";
const String LASTNAME = "last_name";

const String CLIENTID = "client_id";
const String SIGNATURE = "signature";
const String AVATAR = "avatar";
const String URL = "url";
const String HASACCEPTED = "has_accepted_terms";
const String GOOGLETOKEN = "google_token";
const String USERNAME_OR_EMAIL = "username_or_email";
const String ACCESSTOKEN = "access_token";

//Translation map keys

const String FIRSTNAME_ = 'firstname';
const String LASTNAME_ = "lastname";
const String USERNAME_ = "username";
const String EMAIL_ = "email";
const String PASSWORD_ = "password";
const String WELCOME_ = "welcome";
const String DEFAULT_AVATAR = "https://www.ekahiornish.com/wp-content/uploads/2018/07/default-avatar-profile-icon-vector-18942381.jpg";

//signup values


//Sharedpreference Key

const String USERTOKEN = "usertoken";
const String FIRST_TIME = "firstTime";
const String SELECTED_TEAM = "selectedTeam";
const String SHARED_TEAMS = 'teams';

