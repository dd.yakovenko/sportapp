
import 'dart:async';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_twitter_login/flutter_twitter_login.dart';

import 'Utils.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import '../Model/InstaToken.dart';
import 'dart:convert';

class API {
  Future<bool> facebookInfo() async {
    final facebookLogin = FacebookLogin();
    final result = await facebookLogin
        .logInWithReadPermissions(['email', 'public_profile']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        print("LoggedIn");
        final _auth = FirebaseAuth.instance;
        FacebookAccessToken myToken = result.accessToken;

        AuthCredential credential =
            FacebookAuthProvider.getCredential(accessToken: myToken.token);

        FirebaseUser firebaseUser =
            await _auth.signInWithCredential(credential);

        Utils.registerModel.txtFieldValue = false;
        Utils.registerModel.source = "facebook";
        Utils.registerModel.email = firebaseUser.email;
        //    Utils.registerModel.googleToken = firebaseUser.idToken;
        Utils.registerModel.name = firebaseUser.displayName;
        Utils.registerModel.uuid = firebaseUser.uid;

        break;
      case FacebookLoginStatus.cancelledByUser:
        print("CancelledByUser");
        break;
      case FacebookLoginStatus.error:
        Utils.showMessage(FacebookLoginStatus.error.toString());
        print("Error12345");
        print(result.errorMessage);
        break;
    }
    return true;
  }

  Future<bool> googleInfo() async {
    print("error");

    try {
      final _auth = await FirebaseAuth.instance;

      print("autttthhhh");

      GoogleSignInAccount googleSignInAccount = await GoogleSignIn().signIn();
      final GoogleSignInAuthentication googleAuth =
          await googleSignInAccount.authentication;

      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      final FirebaseUser user = await _auth.signInWithCredential(credential);

      print(user.email);

      Utils.registerModel.txtFieldValue = false;
      Utils.registerModel.source = "google";
      Utils.registerModel.email = user.email;
      Utils.registerModel.googleToken = googleAuth.idToken;
      Utils.registerModel.name = user.displayName;
      final FirebaseUser currentUser = await _auth.currentUser();

      Utils.registerModel.uuid = currentUser.uid;

      assert(user.uid == currentUser.uid);

      print('signInWithGoogle succeeded: $user ${Utils.registerModel.uuid}');
      print(user.email);

      print("google signed in user: ${user?.email}");
    } catch (error) {
      print("google sign in error: $error"); // error is printed here
    }

    return true;
  }

  var _twitterLogin = new TwitterLogin(
    consumerKey: '4eSZ6XeZcQGkVuJUpQa8Hg',
    consumerSecret: 'tWbu3DS9ePLgW3mHZ2Nz28Vk0nlenins2QIrvw',
  );
  Future<bool> twitterInfo() async {
    final TwitterLoginResult result = await _twitterLogin.authorize();

    switch (result.status) {
      case TwitterLoginStatus.loggedIn:
        var session = result.session;
        final _auth = FirebaseAuth.instance;

        AuthCredential credential =
        TwitterAuthProvider.getCredential(authToken: session.token, authTokenSecret: session.secret);

        FirebaseUser firebaseUser =
        await _auth.signInWithCredential(credential);

        Utils.registerModel.txtFieldValue = false;
        Utils.registerModel.source = "twitter";
        Utils.registerModel.email = firebaseUser.email;
        //    Utils.registerModel.googleToken = firebaseUser.idToken;
        Utils.registerModel.name = firebaseUser.displayName;
        Utils.registerModel.uuid = firebaseUser.uid;
        break;
      case TwitterLoginStatus.cancelledByUser:
        Utils.showMessage('Twitter login cancelled.');
        print("CancelledByUser");
        break;
      case TwitterLoginStatus.error:
        Utils.showMessage(result.errorMessage);
        print(result.errorMessage);
        break;
    }
    return true;
  }

  Future<bool> instagramInfo() async {
    const appId = '113bb65c032f42f9841f7856987dcf97';
    const appSecret = 'db7214fb86ad4cca8e6033c0f03a1532';
    Stream<String> onCode = await _instaRestApi();
    String url =
        "https://api.instagram.com/oauth/authorize?client_id=$appId&redirect_uri=http://localhost:8585&response_type=code";
    final flutterWebviewPlugin = new FlutterWebviewPlugin();
    flutterWebviewPlugin.launch(url);
    final String code = await onCode.first;
    final http.Response response = await http.post(
        "https://api.instagram.com/oauth/access_token",
        body: {"client_id": appId, "redirect_uri": "http://localhost:8585", "client_secret": appSecret,
          "code": code, "grant_type": "authorization_code"});
    flutterWebviewPlugin.close();
    var token = new InstaToken.fromMap(json.decode(response.body));
    if(token != null) {
      Utils.registerModel.txtFieldValue = false;
      Utils.registerModel.source = "instagram";
      //Utils.registerModel.email = token.;
      Utils.registerModel.name = token.full_name;
      Utils.registerModel.uuid = token.id;
    }
    else {
      return false;
    }
    return true;
  }

  Future<Stream<String>> _instaRestApi() async {
    final StreamController<String> onCode = new StreamController();
    HttpServer server =
    await HttpServer.bind(InternetAddress.loopbackIPv4, 8585);
    server.listen((HttpRequest request) async {
      final String code = request.uri.queryParameters["code"];
      request.response
        ..statusCode = 200
        ..headers.set("Content-Type", ContentType.html.mimeType)
        ..write("<html><h1>You can now close this window</h1></html>");
      await request.response.close();
      await server.close(force: true);
      onCode.add(code);
      await onCode.close();
    });
    return onCode.stream;
  }
}
