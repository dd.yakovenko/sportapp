class UserModel {
  String uuid;

  String _favoriteTeam, _favoriteTeamName, _avatar, _name, _surname, _dob, _location;
  bool _isAdmin;
  String googleToken;
  bool txtFieldValue;
  String source;
  String email;
  int _favoriteTeamId;
  bool _isVerified;

  UserModel();

  String get name => _name;

  String get avatar => _avatar;

  String get favoriteTeam => _favoriteTeam;

  int get favoriteTeamId => _favoriteTeamId;

  String get favoriteTeamName => _favoriteTeamName;

  bool get isAdmin => _isAdmin;

  set avatar(String value) => _avatar = value;

  set isAdmin(bool value) => _isAdmin = value;

  set name(String value) => _name = value;

  set favoriteTeam(String value) => _favoriteTeam = value;

  set favoriteTeamId(int value) => _favoriteTeamId = value;

  set favoriteTeamName(String value) => _favoriteTeamName = value;

  String get surname => _surname;
  set surname(String value) => _surname = value;

  String get dob => _dob;
  set dob(String value) => _dob = value;

  String get location => _location;
  set location(String value) => _location = value;

  bool get isVerified => _isVerified;
  set isVerified(bool value) => _isVerified = value;
}
